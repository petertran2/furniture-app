import React, { useState, useEffect } from 'react'
import ItemList from '../shared/itemList'

export default function Others({ navigation }) {

  const [items, setItems] = useState([])
  
  useEffect(() => {
    fetch('https://pt-furniture-backend.herokuapp.com/api/v1/others')
      .then(response => response.json())
      .then(responseJson => {
        setItems(responseJson.map(el => ({...el, id: `Other${el.id}`})))
      })
      .catch(error => {
        return <Text>There was an error.</Text>
      })
  }, [])
  
  return (
    <ItemList data={items} itemPath='Other' navigation={navigation} />
  )
}
