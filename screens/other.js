import React from 'react'
import Item from '../shared/item'

export default function Other({ navigation, screenProps }) {
  return (
    <Item navigation={navigation} screenProps={screenProps} />
  )
}
