import React, { useState, useEffect } from 'react'
import ItemList from '../shared/itemList'

export default function Chairs({ navigation }) {

  const [items, setItems] = useState([])
  
  useEffect(() => {
    fetch('https://pt-furniture-backend.herokuapp.com/api/v1/chairs')
      .then(response => response.json())
      .then(responseJson => {
        setItems(responseJson.map(el => ({...el, id: `Chair${el.id}`})))
      })
      .catch(error => {
        return <Text>There was an error.</Text>
      })
  }, [])
  
  return (
    <ItemList data={items} itemPath='Chair' navigation={navigation} />
  )
}
