import React, { useState, useEffect } from 'react'
import ItemList from '../shared/itemList'

export default function Beds({ navigation }) {

  const [items, setItems] = useState([])
  
  useEffect(() => {
    fetch('https://pt-furniture-backend.herokuapp.com/api/v1/beds')
      .then(response => response.json())
      .then(responseJson => {
        setItems(responseJson.map(el => ({...el, id: `Bed${el.id}`})))
      })
      .catch(error => {
        return <Text>There was an error.</Text>
      })
  }, [])
  
  return (
    <ItemList data={items} itemPath='Bed' navigation={navigation} />
  )
}
