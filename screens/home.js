import React from 'react'
import { ScrollView, ImageBackground, Text, View } from 'react-native'
import { styles } from '../styles/global'

export default function Home({ navigation }) {
  return (
    <ScrollView style={styles.container}>
    
      <ImageBackground
        source={require('../assets/home/livingroom.jpg')}
        style={styles.homeRoom}
      >
        <Text style={styles.imageText}>Furniture App</Text>
      </ImageBackground>
      
      <View style={styles.categories}>
      
        <View style={styles.col1}>
          <ImageBackground
            source={require('../assets/home/chair.jpg')}
            style={styles.homeImage}
          >
            <Text
              style={styles.imageText}
              onPress={() => navigation.navigate('Chairs')}
            >Chairs</Text>
          </ImageBackground>
        </View>
        
        <View style={styles.col2}>
          <ImageBackground
            source={require('../assets/home/table.jpg')}
            style={styles.homeImage}
          >
            <Text
              style={styles.imageText}
              onPress={() => navigation.navigate('Tables')}
            >Tables</Text>
          </ImageBackground>
        </View>
        
        <View style={styles.colFull}>
          <ImageBackground
            source={require('../assets/home/sofa.jpg')}
            style={styles.homeImage}
          >
            <Text
              style={styles.imageText}
              onPress={() => navigation.navigate('Sofas')}
            >Sofas</Text>
          </ImageBackground>
        </View>
        
        <View style={styles.col2}>
          <ImageBackground
            source={require('../assets/home/bed.jpg')}
            style={styles.homeImage}
          >
            <Text
              style={styles.imageText}
              onPress={() => navigation.navigate('Beds')}
            >Beds</Text>
          </ImageBackground>
        </View>
        
        <View style={styles.col1}>
          <ImageBackground
            source={require('../assets/home/other.jpg')}
            style={styles.homeImage}
          >
            <Text
              style={styles.imageText}
              onPress={() => navigation.navigate('Others')}
            >Other</Text>
          </ImageBackground>
        </View>
      
      </View>
    
    </ScrollView>
  )
}
