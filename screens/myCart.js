import React, { useState } from 'react'
import { View, FlatList, TouchableOpacity, Text } from 'react-native'
import { styles } from '../styles/global'
import CartItem from '../shared/cartItem'

export default function MyCart({ navigation, screenProps }) {
  const [total, setTotal] = useState(
    screenProps.cart.map(el => el.price).reduce((a, b) => a + b, 0)
  )

  return (
    <View style={styles.container}>
      <View style={styles.cartItemList}>
        <FlatList
          data={screenProps.cart}
          renderItem={({ item }) => (
            <CartItem
              item={item}
              screenProps={screenProps}
              setTotal={setTotal}
            />
          )}
        />
      </View>
      <View style={total <= 0 ? [styles.bottomView, styles.disabled] : 
        styles.bottomView}
      >
        <TouchableOpacity
          disabled={total <= 0}
          onPress={() => navigation.navigate('Checkout', {
            total, cart: screenProps.cart
          })}
        >
          <Text style={styles.bottomText}>Go to Checkout - ${ total }</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}
