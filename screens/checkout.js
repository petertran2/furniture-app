import React from 'react'
import { View, Text, TextInput, Button, 
  TouchableWithoutFeedback, Keyboard } from 'react-native'
import { styles, themeColor } from '../styles/global'
import { Formik } from 'formik'

export default function Checkout({ navigation, screenProps }) {
  return (
    <TouchableWithoutFeedback style={styles.container} 
      onPress={Keyboard.dismiss}
    >
      <Formik
        initialValues={{name: '', email: '', address: '', city: '', state: '',
          zip: '', cardnumber: ''}}
        onSubmit={(values, actions) => {
          fetch('https://pt-furniture-backend.herokuapp.com/api/v1/orders', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              ...values,
              total: navigation.getParam('total'),
              cart: navigation.getParam('cart').toString()
            })
          })
          actions.resetForm()
          screenProps.setCart([])
          navigation.navigate('Home')
        }}
      >
        {props => (
          <View>
            <Text style={styles.formTotal}>
              Total: ${ navigation.getParam('total') }
            </Text>
            <TextInput
              style={styles.input}
              placeholder='Name'
              onChangeText={props.handleChange('name')}
              value={props.values.name}
            />
            <TextInput
              style={styles.input}
              placeholder='Email'
              onChangeText={props.handleChange('email')}
              value={props.values.email}
            />
            <TextInput
              style={styles.input}
              placeholder='Address'
              onChangeText={props.handleChange('address')}
              value={props.values.address}
            />
            <TextInput
              style={styles.input}
              placeholder='City'
              onChangeText={props.handleChange('city')}
              value={props.values.city}
            />
            <TextInput
              style={styles.input}
              placeholder='State'
              onChangeText={props.handleChange('state')}
              value={props.values.state}
            />
            <TextInput
              style={styles.input}
              placeholder='Zip'
              onChangeText={props.handleChange('zip')}
              value={props.values.zip}
              keyboardType='numeric'
            />
            <TextInput
              style={styles.input}
              placeholder='Card Number'
              onChangeText={props.handleChange('cardnumber')}
              value={props.values.cardnumber}
              keyboardType='numeric'
            />
            <Button color={themeColor} title='Confirm Order' 
              onPress={props.handleSubmit}
            />
          </View>
        )}
      </Formik>
    </TouchableWithoutFeedback>
  )
}
