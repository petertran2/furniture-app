import { StyleSheet } from 'react-native'

export const themeColor = '#36495e'

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  menu: {
    paddingLeft: 12,
    color: 'white'
  },
  cart: {
    paddingRight: 12,
    color: 'white'
  },
  homeRoom: {
    width: '100%',
    height: 300,
    justifyContent: 'center'
  },
  imageText: {
    alignSelf: 'center',
    fontSize: 28,
    backgroundColor: themeColor,
    textAlign: 'center',
    padding: 10,
    color: 'white',
    fontWeight: 'bold',
    borderRadius: 5
  },
  categories: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    padding: 5
  },
  col1: {
    flex: 1,
    padding: 5
  },
  col2: {
    flex: 2,
    padding: 5
  },
  colFull: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5
  },
  homeImage: {
    width: '100%',
    height: 200,
    alignItems: 'center',
    justifyContent: 'center'
  },
  itemScrollView: {
    marginBottom: 50
  },
  itemImage: {
    width: '100%',
    height: 300
  },
  itemView: {
    marginVertical: 14
  },
  itemName: {
    fontSize: 20,
    textAlign: 'center',
    color: themeColor
  },
  itemText: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingHorizontal: 50,
    marginTop: 10
  },
  itemPrice: {
    fontWeight: 'bold',
    fontSize: 16,
    color: themeColor,
    justifyContent: 'center',
    alignSelf: 'center'
  },
  itemLink: {
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 16
  },
  inStock: {
    color: 'green',
    fontWeight: 'bold',
    fontSize: 16
  },
  outStock: {
    color: 'red',
    fontWeight: 'bold',
    fontSize: 16
  },
  itemDesc: {
    lineHeight: 22,
    fontSize: 16,
    marginTop: 10,
    marginHorizontal: 40,
    textAlign: 'justify',
    color: themeColor
  },
  bottomView: {
    width: '100%', 
    height: 50, 
    backgroundColor: themeColor, 
    justifyContent: 'center', 
    alignItems: 'center',
    position: 'absolute',
    bottom: 0
  },
  bottomText: {
    color: 'white',
    fontSize: 22
  },
  disabled: {
    opacity: 0.3
  },
  cartItemList: {
    marginTop: 15,
    marginHorizontal: 15,
    marginBottom: 50
  },
  cartItem: {
    width: '100%',
    borderColor: 'rgba(255, 255, 255, 0.2)',
    borderWidth: 1,
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: {width: 10, height: 10},
    shadowOpacity: 0.8,
    shadowRadius: 3,
    elevation: 1,
    padding: 15,
    marginBottom: 15
  },
  cartImage: {
    width: '100%',
    height: 200,
    marginBottom: 10
  },
  cartItemText: {
    flexDirection: 'row',
    marginTop: 5,
    justifyContent: 'space-between',
    marginLeft: 16
  },
  cartQuantity: {
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  cartItemButton: {
    backgroundColor: themeColor,
    width: 30,
    height: 30,
    padding: 5,
    marginHorizontal: 5,
    justifyContent: 'center'
  },
  cartItemButtonSign: {
    color: 'white',
    fontSize: 20,
    alignSelf: 'center'
  },
  cartItemAmount: {
    justifyContent: 'center',
    alignSelf: 'center',
    marginHorizontal: 5
  },
  cartDelete: {
    width: 60,
    backgroundColor: 'red',
    justifyContent: 'center'
  },
  cartDeleteText: {
    color: 'white',
    fontWeight: 'bold',
    alignSelf: 'center'
  },
  formTotal: {
    paddingTop: 10,
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 20
  },
  input: {
    margin: 10,
    borderWidth: 1,
    borderColor: themeColor,
    padding: 10,
    fontSize: 18,
    borderRadius: 6
  }
})
