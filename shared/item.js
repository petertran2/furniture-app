import React from 'react'
import { ScrollView, View, Text, Image, TouchableOpacity } from 'react-native'
import { styles } from '../styles/global'

export default function Item({ navigation, screenProps }) {
  const quantity = navigation.getParam('quantity')
  const disable = !quantity ||
    !!screenProps.cart.find(obj => obj.id == navigation.getParam('id'))

  return (
    <View style={styles.container}>
      <ScrollView style={styles.itemScrollView}>
        <Image source={{uri: navigation.getParam('image')}} style={styles.itemImage} />
        <View style={styles.itemView}>
          <Text style={styles.itemName}>{ navigation.getParam('name') }</Text>
          <View style={styles.itemText}>
            <Text style={styles.itemPrice}>${ navigation.getParam('price') }</Text>
            <Text style={quantity ? styles.inStock : styles.outStock}>
              { quantity ? 'In Stock' : 'Out of Stock' }
            </Text>
          </View>
          <Text style={styles.itemDesc}>{ navigation.getParam('desc') }</Text>
        </View>
      </ScrollView>
      <View style={disable ? [styles.bottomView, styles.disabled] : styles.bottomView }>
        <TouchableOpacity
          disabled={disable}
          onPress={() => {
            screenProps.setCart(prevCart => [...prevCart, 
              {...navigation.state.params, amount: 1}
            ])
            navigation.navigate('Cart')
          }}
        >
          <Text style={styles.bottomText}>Add to Cart</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}
