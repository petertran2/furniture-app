import React, { useState } from 'react'
import { View, Image, Text, TouchableOpacity } from 'react-native'
import { styles } from '../styles/global'

export default function CartItem({ item, screenProps, setTotal }) {
  const noMinus = item['amount'] <= 1
  const noPlus = item['amount'] >= item['quantity']
  const disabledStyle = reachedLimit => reachedLimit ?
    [styles.cartItemButton, styles.disabled] : styles.cartItemButton
  
  return (
    <View style={styles.cartItem}>
      <Image source={{uri: item['image']}} style={styles.cartImage} />
      <Text style={styles.itemName}>{ item['name'] }</Text>
      <View style={styles.cartItemText}>
        <Text style={styles.itemPrice}>${ item['price'] }</Text>
        <View style={styles.cartQuantity}>
          <TouchableOpacity
            style={disabledStyle(noMinus)}
            disabled={noMinus}
            onPress={() => {
              screenProps.setCart(prevCart => (
                prevCart.map(product => (
                  product.id == item['id'] ? 
                  {...product, amount: (item['amount'] - 1)} : product
                ))
              ))
              setTotal(prevTotal => prevTotal - item['price'])
            }}
          >
            <Text style={styles.cartItemButtonSign}>-</Text>
          </TouchableOpacity>
          <Text style={styles.cartItemAmount}>{ item['amount'] }</Text>
          <TouchableOpacity
            style={disabledStyle(noPlus)}
            disabled={noPlus}
            onPress={() => {
              screenProps.setCart(prevCart => (
                prevCart.map(product => (
                  product.id == item['id'] ? 
                  {...product, amount: (item['amount'] + 1)} : product
                ))
              ))
              setTotal(prevTotal => prevTotal + item['price'])
            }}
          >
            <Text style={styles.cartItemButtonSign}>+</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={styles.cartDelete}
          onPress={() => {
            setTotal(prevTotal => prevTotal - (item['price'] * item['amount']))
            screenProps.setCart(prevCart => (
              prevCart.filter(product => product.id != item['id']))
            )
          }}
        >
          <Text style={styles.cartDeleteText}>Remove</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}
