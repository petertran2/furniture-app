import React from 'react'
import { View, FlatList, Image, Text } from 'react-native'
import { styles } from '../styles/global'

export default function ItemList({ data, itemPath, navigation }) {
  return (
    <View style={styles.container}>
      <FlatList
        data={data}
        renderItem={({ item }) => (
          <View>
            <Image source={{uri: item.image}} style={styles.itemImage} />
            <View style={styles.itemView}>
              <Text style={styles.itemName}>{ item.name }</Text>
              <View style={styles.itemText}>
                <Text style={styles.itemPrice}>${ item.price }</Text>
                <Text style={styles.itemLink} onPress={() => navigation.navigate(itemPath, item)}>See Details</Text>
              </View>
            </View>
          </View>
        )}
        keyExtractor={item => item.id}
      />
    </View>
  )
}
