import React, { useState } from 'react'
import Navigator from './routes/drawer'

export default function App() {
  const [cart, setCart] = useState([])
  
  return (
    <Navigator screenProps={{cart, setCart}} />
  )
}
