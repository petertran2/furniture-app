import { createDrawerNavigator } from 'react-navigation-drawer'
import { createAppContainer } from 'react-navigation'
import HomeStack from './homeStack'
import SofaStack from './sofaStack'
import BedStack from './bedStack'
import ChairStack from './chairStack'
import TableStack from './tableStack'
import OtherStack from './otherStack'
import { themeColor } from '../styles/global'

export default createAppContainer(createDrawerNavigator({
  Home: {
    screen: HomeStack
  },
  Sofas: {
    screen: SofaStack
  },
  Beds: {
    screen: BedStack
  },
  Chairs: {
    screen: ChairStack
  },
  Tables: {
    screen: TableStack
  },
  Others: {
    screen: OtherStack
  }
}, {
  drawerBackgroundColor: themeColor,
  contentOptions: {
    activeBackgroundColor: 'white',
    activeTintColor: themeColor,
    inactiveTintColor: 'white',
    labelStyle: {
      fontSize: 20
    }
  }
}))
