import { createStackNavigator } from 'react-navigation-stack';
import Chairs from '../screens/chairs'
import Chair from '../screens/chair'

export default createStackNavigator({
  Chairs: {
    screen: Chairs
  },
  Chair: {
    screen: Chair
  }
})
