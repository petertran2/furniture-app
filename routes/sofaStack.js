import { createStackNavigator } from 'react-navigation-stack';
import Sofas from '../screens/sofas'
import Sofa from '../screens/sofa'

export default createStackNavigator({
  Sofas: {
    screen: Sofas
  },
  Sofa: {
    screen: Sofa
  }
})
