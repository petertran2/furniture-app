import React from 'react'
import { createStackNavigator } from 'react-navigation-stack';
import Home from '../screens/home'
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import { themeColor, styles } from '../styles/global'
import Cart from '../screens/myCart'
import Checkout from '../screens/checkout'
import Sofas from '../screens/sofas'
import Sofa from '../screens/sofa'
import Beds from '../screens/beds'
import Bed from '../screens/bed'
import Chairs from '../screens/chairs'
import Chair from '../screens/chair'
import Tables from '../screens/tables'
import Table from '../screens/table'
import Others from '../screens/others'
import Other from '../screens/other'

export default createStackNavigator({
  Home: {
    screen: Home,
    navigationOptions: ({ navigation }) => ({
      headerLeft: () => <MaterialIcon name='menu' size={28} style={styles.menu} onPress={() => navigation.openDrawer()} />
    })
  },
  Cart: {
    screen: Cart,
    navigationOptions: {
      title: 'My Cart'
    }
  },
  Checkout: {
    screen: Checkout
  },
  Sofas: {
    screen: Sofas
  },
  Sofa: {
    screen: Sofa
  },
  Beds: {
    screen: Beds
  },
  Bed: {
    screen: Bed
  },
  Chairs: {
    screen: Chairs
  },
  Chair: {
    screen: Chair
  },
  Tables: {
    screen: Tables
  },
  Table: {
    screen: Table
  },
  Others: {
    screen: Others
  },
  Other: {
    screen: Other
  }
}, {defaultNavigationOptions: ({ navigation }) => ({
  headerTitleAlign: 'center',
  headerRight: () => <MaterialIcon name='cart' size={28} style={styles.cart} onPress={() => navigation.navigate('Cart')} />,
  headerStyle: {
    backgroundColor: themeColor
  },
  headerTitleStyle: {
    color: 'white',
    fontSize: 22
  },
  headerTintColor: 'white'
})})
