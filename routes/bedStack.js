import { createStackNavigator } from 'react-navigation-stack';
import Beds from '../screens/beds'
import Bed from '../screens/bed'

export default createStackNavigator({
  Beds: {
    screen: Beds
  },
  Bed: {
    screen: Bed
  }
})
