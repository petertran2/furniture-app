import { createStackNavigator } from 'react-navigation-stack';
import Tables from '../screens/tables'
import Table from '../screens/table'

export default createStackNavigator({
  Tables: {
    screen: Tables
  },
  Table: {
    screen: Table
  }
})
