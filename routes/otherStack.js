import { createStackNavigator } from 'react-navigation-stack';
import Others from '../screens/others'
import Other from '../screens/others'

export default createStackNavigator({
  Others: {
    screen: Others
  },
  Other: {
    screen: Other
  }
})
