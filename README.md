<img align="right" src="https://gitlab.com/petertran2/furniture-app/-/raw/master/README_assets/Peek%202020-02-10%2017-23.gif">

### Furniture App

This mobile app for furniture products is a React Native app that displays listings of different kinds of furniture from an e-commerce shop, ranging from sofas to chairs to beds. To populate the listings, it receives data from an API back-end written with Ruby on Rails. The app includes a cart and a checkout system that sends the customer's order requests to the API.

For more information about the API, please visit [its repository](https://gitlab.com/petertran2/furniture-app-backend).

#### Technologies

- React Native (front end)
- React Navigation
- Vector Icons
- Formik
- Ruby on Rails (back end)
- PostgreSQL
- JSON
- Heroku

##### Author
Peter Tran